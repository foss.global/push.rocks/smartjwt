/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartjwt',
  version: '2.0.2',
  description: 'a package for handling jwt'
}
